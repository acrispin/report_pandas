# Use latest Python runtime as a parent image
FROM microsoft/pyodbc3

# Meta-data
LABEL maintainer="Antonio Crispin <antoniocrispin@gmail.com>" \
      description="Prueba de conexion sql server desde linux \
      usando pyodbc"

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install the required libraries
RUN pip --no-cache-dir install -r /app/requirements.txt

# Make port 8888 available to the world outside this container
EXPOSE 8888

# Create mountpoint
VOLUME /app

# Run jupyter when container launches
CMD ["jupyter", "notebook", "--ip='*'", "--port=8888", "--no-browser", "--allow-root"]
