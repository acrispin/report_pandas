from bokeh.io import curdoc
from bokeh.models import Div
import bokeh

d = Div(text='Hello from bokeh {}'.format(bokeh.__version__))

curdoc().add_root(d)

"""
https://github.com/lukauskas/docker-bokeh/blob/master/docker/app/main.py
$ bokeh serve --show myapp3.py
$ bokeh serve --port 8889 --show myapp3.py
"""